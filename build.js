var compressor = require("node-minify");
var fs = require("fs");
const gzipSize = require("gzip-size");
const prettyBytes = require("pretty-bytes");

const srcFolder = "./src";
const namespace = "edrastyle";
const names = ["core", "margin", "padding", "spinner", "switch"];
const distFolder = "./dist";

for (var i = 0; i < names.length; i++) {
  const outputFileName = `${distFolder}/${namespace}.${names[i]}.min.css`;
  compressor.minify({
    compressor: "crass",
    input: `${srcFolder}/${names[i]}.css`,
    output: outputFileName,
    callback: function(err, min) {
      console.log("✅  " + outputFileName);
      const stats = fs.statSync(outputFileName);
      const size = gzipSize.fileSync(outputFileName);
      const bytes = prettyBytes(stats.size);
      console.log(
        `\tfile size:\t${bytes}\n\tgzipped size:\t${prettyBytes(size)}\n`
      );
    }
  });
}
